pub usingnamespace @cImport({
    @cInclude("SDL2/SDL.h");
    @cInclude("SDL2/SDL_revision.h");
    @cInclude("SDL2/SDL_image.h");
    @cInclude("SDL2_gfxPrimitives.h");
    @cInclude("SDL2/SDL_ttf.h");
});
