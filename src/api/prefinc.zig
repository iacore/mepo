const Mepo = @import("../Mepo.zig");
const types = @import("../types.zig");
const std = @import("std");
const utildbg = @import("../util/utildbg.zig");
const p = @import("../util/utilprefs.zig");

pub const spec = .{
    .name = "prefinc",
    .desc = "Increase or decrease a preference by the given delta value.",
    .args = (&[_]types.MepoFnSpecArg{
        .{ .tag = .Text, .name = "prefname", .desc = "Preference name" },
        .{ .tag = .Number, .name = "delta", .desc = "Delta value to increment/decrement (positive or negative)" },
    })[0..],
    .execute = execute,
};

fn execute(_: *Mepo, args: [types.MepoFnNargs]types.MepoArg) !void {
    const name = args[0].Text;
    const delta = args[1].Number;

    if (p.lookup_pref_from_string(name)) |key| {
        const val = p.get(key).u;
        p.set_n(key, @intToFloat(f64, val) + delta);
    }
}
